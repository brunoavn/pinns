###################################################################
############# Resolvendo Burgers Com Viscosidade  #################
###################################################################

import sys
sys.path.insert(0, './Solver/')

import numpy as np
import burgers_viscous_time_exact1 as BurVis
import matplotlib.pyplot as plt

nu = 0.01/np.pi             # Viscosidade
vxn = 100                   # Número de elementos de X
vx = np.linspace(-1,1, vxn) # Pontos X
vtn = 256                   # Número de pontos 
vt = np.linspace(0,1, vtn)  # Pontos t
qn = 400                    # Grau do polinômio de Hermite

A = BurVis.burgers_viscous_time_exact1(nu, vxn, vx, vtn, vt, qn)

name = 'Sol_BurgersVis_nu{}_nel{}x{}_hermite{}.txt'.format(nu,vxn,vtn,qn)

np.savetxt(name, A)
np.savetxt('./Data/tempo.txt', vt)
np.savetxt('./Data/eixo.txt', vx)

## -- Plotando -- ##
plt.imshow(A, cmap='rainbow', extent=(vt.min(), vt.max(),vx.min(), vx.max()), aspect='auto')

#plt.show()
plt.savefig('./Figures/Sol_Hermite_grau{}_pontos{}X{}.png'.format(qn, vxn, vtn), dpi= 300)
