
from numpy import savetxt

class Exporter:
    
    def __init__(self, data, name):
        self.data = data
        self.name = name
    
    def Export(self):
        savetxt(self.name, self.data)
        
        