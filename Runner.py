#import sys
#sys.path.insert(0,'./Advecção/')
#sys.path.insert(1,'./BurgersInviscido/')
#sys.path.insert(2,'./BurgersViscoso/')
#sys.path.append('./aux/')
import os

def Runner(models, optimizer, epochs, lr, layers = []):

    for i in models:
        NonDefaultOpt = 0

        if i == 'Advec':
            os.chdir('./Advecção/')
            from Advecção import NN_Advec as NN
            print('Rodando o modelo {}'.format(i))
        
        elif i == 'Advec_Adam':
            os.chdir('./Advecção/')
            from Advecção import NN_Advec_Adam as NN
            print('Rodando o modelo {}'.format(i))
            NonDefaultOpt = 1

        elif i == 'BurgersVis':
            os.chdir('./BurgersViscoso/')
            from BurgersViscoso import NN_Burgers as NN
            print('Rodando o modelo {}'.format(i))

        elif i == 'BurgersVis_Adam':
            os.chdir('./BurgersViscoso/')
            from BurgersViscoso import NN_Burgers_Adam as NN
            print('Rodando o modelo {}'.format(i))
            NonDefaultOpt = 1
        
        elif i == 'BurgersInv':
            os.chdir('./BurgersInviscido/')
            from BurgersInviscido import NN_BurgersInv as NN
            print('Rodando o modelo {}'.format(i))

        elif i == 'BurgersInv_Adam':
            os.chdir('./BurgersInviscido/')
            from BurgersInviscido import NN_BurgersInv_Adam as NN
            print('Rodando o modelo {}'.format(i))
            NonDefaultOpt = 1
        elif i == 'DeepPhysics':
            os.chdir('./DeepPhysics/')
            import Deep_BurgersVis as NN
            print('Rodando o modelo {}'.format(i))
            NonDefaultOpt = 2
        else:
            print('O modelo {} não está disponível, selecione outro modelo'.format(i))
            continue
        
        if NonDefaultOpt == 1:
            for i in epochs:
                NN.RunNN(Epochs = i, Optimizer = optimizer, lr = lr)
        elif NonDefaultOpt == 2:
            for i in layers:
                NN.RunNN(i)
        else:
            NN.RunNN()

if __name__ == "__main__":

    models = ['DeepPhysics']
    optimizer = 'Adam'
    epochs = [10]
    lr = 0.0014
    layers = [[3, 50, 50, 50,1], [3, 50, 50, 50, 50,1], [3, 50, 50, 50, 50, 50,1],
              [3,100,100,100,1], [3,100,100,100,100,1], [3,100,100,100,100,100,1],
              [3,150,150,150,1], [3,150,150,150,150,1], [3,150,150,150,150,150,1],
              [3,200,200,200,1], [3,200,200,200,200,1], [3,200,200,200,200,200,1]]

    Runner(models, optimizer, epochs, lr, layers)
