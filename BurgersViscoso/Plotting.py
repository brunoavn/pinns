import sys
sys.path.insert(0, '../aux/')

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from plotting import newfig, savefig


elapsed = input('Tempo da Rede neural: ')


x0 = -8
xf = 8
t0 = 0 
tf = 10

Exact = np.loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosExatos.txt'.format(elapsed))
Exact_sol = Exact

U_pred =  np.loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosPreditos.txt'.format(elapsed))

t = np.loadtxt('./Data/Sol_BurgersVis_T.txt')
x = np.loadtxt('./Data/Sol_BurgersVis_X.txt')

sample1 = int(Exact[:,1].size*0.25)
sample2 = int(Exact[:,1].size*0.50)
sample3 = int(Exact[:,1].size*0.75)


fig, ax = newfig(1.0, 1.1)
ax.axis('off')

####### Row 0: u(t,x) ##################    
gs0 = gridspec.GridSpec(1, 2)
gs0.update(top=1-0.06, bottom=1-1/3, left=0.15, right=0.85, wspace=0)
ax = plt.subplot(gs0[:, :])

h = ax.imshow(U_pred, interpolation='nearest', cmap='rainbow', 
              extent=[t0, tf, x0, xf], 
              origin='lower', aspect='auto')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
fig.colorbar(h, cax=cax)

#ax.plot(X_u_train[:,1], X_u_train[:,0], 'kx', label = 'Data (%d pontos)' % (u_train.shape[0]), markersize = 4, clip_on = False)

line = np.linspace(x0, xf, 2)[:,None]
ax.plot(t[sample1]*np.ones((2,1)), line, 'w-', linewidth = 1)
ax.plot(t[sample2]*np.ones((2,1)), line, 'w-', linewidth = 1)
ax.plot(t[sample3]*np.ones((2,1)), line, 'w-', linewidth = 1)    

ax.set_xlabel('$t$')
ax.set_ylabel('$x$')
ax.legend(frameon=False, loc = 'best')
ax.set_title('$u(t,x)$', fontsize = 10)

####### Row 1: u(t,x) slices ##################    
gs1 = gridspec.GridSpec(1, 3)
gs1.update(top=1-1/3, bottom=0, left=0.1, right=0.9, wspace=0.5)

ax = plt.subplot(gs1[0, 0])
ax.plot(x,Exact[sample1,:], 'b-', linewidth = 2, label = 'Exato')       
ax.plot(x,U_pred[sample1,:], 'r--', linewidth = 2, label = 'Predito')
ax.set_xlabel('$x$')
ax.set_ylabel('$u(t,x)$')    
ax.set_title('$t = 0.25$', fontsize = 10)
ax.axes.set_aspect(10)


ax = plt.subplot(gs1[0, 1])
ax.plot(x,Exact[sample2,:], 'b-', linewidth = 2, label = 'Exato')       
ax.plot(x,U_pred[sample2,:], 'r--', linewidth = 2, label = 'Predito')
ax.set_xlabel('$x$')
ax.set_ylabel('$u(t,x)$')
ax.set_ylim(-1.1,1.1)
ax.axes.set_aspect(10)
ax.set_title('$t = 0.50$', fontsize = 10)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.35), ncol=5, frameon=False)

ax = plt.subplot(gs1[0, 2])
ax.plot(x,Exact[sample3,:], 'b-', linewidth = 2, label = 'Exato')       
ax.plot(x,U_pred[sample3,:], 'r--', linewidth = 2, label = 'Predito')
ax.set_xlabel('$x$')
ax.set_ylabel('$u(t,x)$')
ax.set_ylim(-1.1,1.1)
ax.axes.set_aspect(10)    
ax.set_title('$t = 0.75$', fontsize = 10)

Name = 'PlotMaziar_BurgersVis_{}'.format(elapsed)
savefig('./Figures/{}'.format(Name))  
plt.clf()
#plot_aux(U_pred, x, t, ('diff', 'previsto', 'instantes'), 'BurgersVis', elapsed, Exact = Exact)

print('Terminado')
