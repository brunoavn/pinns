###################################################################
#################### Lendo a Solução Gerada #######################
###################################################################
from numpy import array
from numpy import loadtxt
from numpy import linspace
from numpy import savetxt

class Reader:
    
    """
    Classe que inicializa o leitor da solução gerada
    
    Argumentos
    =========
        frames : integer
            Número de frames usados na solução.
        time : float
            Tempo final da simulação.
        PATH : string, optional
            Destino onde estão salvas as soluções.
            Padrão é './_output/fort.q'
    
    !!IMPORTANTE!! 
                   O reader lê os 5 primeiros valores,
                   que são apenas informações do solver
                   excluir na hora de usar. 'x[:, 5:]'
                   

    """
    def __init__ (self, frames, time = 10.0, PATH = './_output/fort.q'):
        self.frames = frames
        self.PATH = PATH
        self.time = time
        
        
    def read(self, export = False):
        
        """
        Retorna as soluções em um np.array e o tempo transcorrido pela simulação
        
        """
 
        local = [self.PATH + format(i, '04') for i in range(self.frames + 1)]
        x_i = array([loadtxt(local[a], usecols=0) for a in range(self.frames + 1)])
        timetot =  linspace(0, self.time, self.frames + 1)
        
        if export:
            name = './Data/Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.time)
            savetxt(name, x_i)
        else:
            pass
        
        return x_i, timetot

        
