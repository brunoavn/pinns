###################################################################
################ Resolvendo Equação de Advecção ###################
###################################################################

from clawpack import pyclaw
from clawpack import riemann
from numpy import savetxt
from numpy import linspace

class Advec:
    
    """
    Resolve a eq. de Advecção e exporta os dados para a pasta _output.
    
    Argumentos
    =========
        x0 : float
            Valor inicial do domínio, como o domínio é simétrico x0 = xf.
        tf : float
            Tempo final da simulação.
        frames : integer
            Número de quadros a serem simulados.
        points : integer
            Número de pontos da simulação.
    """
    
    def __init__ (self, x0, tf, frames, points):
        self.x0 = x0
        self.tf = tf
        self.frames = frames
        self.points = points
    
    
    def Solve_Advec(self):
        
        solver = pyclaw.ClawSolver1D(riemann.advection_1D)

        ## -- Condições de Contorno -- ##
        solver.bc_lower[0] = pyclaw.BC.extrap
        solver.bc_upper[0] = pyclaw.BC.extrap

        ## -- Domínio -- ##
        domain = pyclaw.Domain([-self.x0], [self.x0], [self.points])

        ## -- Aplicando a Solução -- ##
        solution = pyclaw.Solution(solver.num_eqn, domain)
        state = solution.state

        xc = state.grid.p_centers[0]

        ## -- Condições iniciais -- ##
        from numpy import sin
        from numpy import pi
        state.q[0,:] = -sin((pi * xc)/8)
        state.problem_data['u'] = 10.0 # Para definir se utiliza-se entropy fix (?)

        ## -- Iniciando o controlador da simulação -- ##
        controller = pyclaw.Controller()
        controller.outdid = './Advec/'
        controller.solution = solution
        controller.solver = solver
        controller.num_output_times = self.frames # número de quadros da simulação
        controller.tfinal = self.tf # tempo total da simulação

        controller.write_aux_init = False
        controller.write_aux_always = False

        controller.solver.all_bcs = pyclaw.BC.extrap

        status = controller.run()

        ## -- Salvando as informações do Grid
        tempo = linspace(0, self.tf, self.frames)
        eixo = linspace(-self.x0, self.x0, self.points)
        savetxt('./Data/tempo[0x{}].txt'.format(self.tf), tempo)
        savetxt('./Data/eixo[{}x{}].txt'.format(-self.x0, self.x0), eixo)
        ## - Final do programa	
        print(40*'#' + '\n' + 11*'-' + 'Solução Finalizada' + 11*'-' + '\n' + 40*'#')


