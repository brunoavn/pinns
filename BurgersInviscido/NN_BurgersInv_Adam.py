# Neste programa rodaremos a NN com o optimizador Adam e limitaremos o número de epochs


import sys
sys.path.insert(0, '../aux/')

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy.interpolate import griddata
from pyDOE import lhs
from plotting import newfig, savefig
from mpl_toolkits.mplot3d import Axes3D
import time
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable

from aux import plot_aux, namer

np.random.seed(1234)
tf.set_random_seed(1234)

class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, X_u, u, X_f, layers, lb, ub, vu, opt, learning):
        
        self.lb = lb
        self.ub = ub
    
        self.x_u = X_u[:,0:1]
        self.t_u = X_u[:,1:2]
        
        self.x_f = X_f[:,0:1]
        self.t_f = X_f[:,1:2]
        
        self.u = u
        
        self.layers = layers
        self.vu = vu
        
        self.opt = opt
        self.learning = learning

        # Initialize NNs
        self.weights, self.biases = self.initialize_NN(layers)
        
        # tf placeholders and graph
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True,
                                                     log_device_placement=True))
        
        self.x_u_tf = tf.placeholder(tf.float32, shape=[None, self.x_u.shape[1]])
        self.t_u_tf = tf.placeholder(tf.float32, shape=[None, self.t_u.shape[1]])        
        self.u_tf = tf.placeholder(tf.float32, shape=[None, self.u.shape[1]])
        
        self.x_f_tf = tf.placeholder(tf.float32, shape=[None, self.x_f.shape[1]])
        self.t_f_tf = tf.placeholder(tf.float32, shape=[None, self.t_f.shape[1]])        
                
        self.u_pred = self.net_u(self.x_u_tf, self.t_u_tf) 
        self.f_pred = self.net_f(self.x_f_tf, self.t_f_tf)         
        
        self.loss = tf.reduce_mean(tf.square(self.u_tf - self.u_pred)) + \
                    tf.reduce_mean(tf.square(self.f_pred))
               
        opt_dict = {
            'Adam' : tf.train.AdamOptimizer,
            'Adagrad' : tf.train.AdagradOptimizer,
            'GradientDescent' : tf.train.GradientDescentOptimizer,
        }       
        self.optimizer = opt_dict[self.opt](self.learning).minimize(self.loss)
        
        init = tf.global_variables_initializer()
        self.sess.run(init)

                
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float32), dtype=tf.float32)
            weights.append(W)
            biases.append(b)        
        return weights, biases
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]        
        xavier_stddev = np.sqrt(2/(in_dim + out_dim))
        return tf.Variable(tf.truncated_normal([in_dim, out_dim], stddev=xavier_stddev), dtype=tf.float32)
    
    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1
        
        H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
            
    def net_u(self, x, t):
        u = self.neural_net(tf.concat([x,t],1), self.weights, self.biases)
        return u
    
    def net_f(self, x,t):
        u = self.net_u(x,t)
        u2 = tf.square(u) 
        u_t = tf.gradients(u, t)[0]
        u_x = tf.gradients(u2, x)[0]
        f = u_t + 0.5*u_x 
        return f
    
    def callback(self, loss):
        print('Loss:', loss)
        
    def train(self, epochs):
        
        init = tf.global_variables_initializer()
        self.sess.run(init)

        tf_dict = {self.x_u_tf: self.x_u, self.t_u_tf: self.t_u, self.u_tf: self.u,
                   self.x_f_tf: self.x_f, self.t_f_tf: self.t_f}
        
        for i in range(epochs):
            self.optimizer.run(session = self.sess, feed_dict = tf_dict )

    def predict(self, X_star):
                
        u_star = self.sess.run(self.u_pred, {self.x_u_tf: X_star[:,0:1], self.t_u_tf: X_star[:,1:2]})  
        f_star = self.sess.run(self.f_pred, {self.x_f_tf: X_star[:,0:1], self.t_f_tf: X_star[:,1:2]})
               
        return u_star, f_star
    
#if __name__ == "__main__": 
def RunNN(Epochs = 10, Optimizer = 'Adam', lr=0.001):    
    vu = 1.0
    noise = 0.0        

    N_u = 100
    N_f = 10000
    layers = [2, 20, 20, 20, 20, 20, 20, 20, 20, 1]

    #Epoch = input('Insira o número de Epochs: ')
    #Epochs = int(Epoch)
   
    t = np.loadtxt('./Data/tempo_tf10.txt').flatten()[:,None]
    x = np.loadtxt('./Data/eixo[-10x10].txt').flatten()[:,None]
    Exac = np.loadtxt('./Data/Sol_Bur_frames-100_time-10.txt').T

    Exact = Exac[:, 5:]

    X, T = np.meshgrid(x,t)
    
    X_star = np.hstack((X.flatten()[:,None], T.flatten()[:,None]))
    u_star = Exact.flatten()[:,None]              
    
    # Doman bounds
    lb = X_star.min(0)
    ub = X_star.max(0)    
        
    xx1 = np.hstack((X[0:1,:].T, T[0:1,:].T))
    uu1 = Exact[0:1,:].T
    xx2 = np.hstack((X[:,0:1], T[:,0:1]))
    uu2 = Exact[:,0:1]
    xx3 = np.hstack((X[:,-1:], T[:,-1:]))
    uu3 = Exact[:,-1:]
    
    X_u_train = np.vstack([xx1, xx2, xx3]) ## Nesta linha estamos definindo quais serão os pontos
    #dos dados fornecidos, os quais utilizaremos no treinamento da rede neural. Temos  300 dados,
    #sendo alguns de t=0, e alguns de x[-10,10].
    X_f_train = lb + (ub-lb)*lhs(2, N_f)
    X_f_train = np.vstack((X_f_train, X_u_train))
    u_train = np.vstack([uu1, uu2, uu3])
    
    idx = np.random.choice(X_u_train.shape[0], N_u, replace=False)
    X_u_train = X_u_train[idx, :]
    u_train = u_train[idx,:]
        
    model = PhysicsInformedNN(X_u_train, u_train, X_f_train, layers, lb, ub, vu, Optimizer, lr)
    
    start_time = time.time()                
    model.train(Epochs)
    elapsed = time.time() - start_time                
    print('Training time: %.4f' % (elapsed))
    
    u_pred, f_pred = model.predict(X_star)
            
    error_u = np.linalg.norm(u_star-u_pred,2)/np.linalg.norm(u_star,2)
    print('Error u: %e' % (error_u))                     

    
    U_pred = griddata(X_star, u_pred.flatten(), (X, T), method='cubic')
    Error = np.abs(Exact - U_pred)

    ######################################################################
    ########################## Saving Results ############################
    ###################################################################### 
    Predito = namer('DadosPreditos', 'BurgInv', elapsed, optimizer = Optimizer, it = Epochs, lr = lr)
    Exato = namer('DadosExatos', 'BurgInv', elapsed, optimizer = Optimizer, it = Epochs, lr = lr)
    
    np.savetxt('./Data/NN_Export/{}.txt'.format(Predito), U_pred)
    np.savetxt('./Data/NN_Export/{}.txt'.format(Exato), Exact)
   

    ######################################################################
    ############################# Plotting ###############################
    ######################################################################    

    # Usando o tamanho do Exact para pegar as mostras igualmente espaçadas do modelo
    sample1 = int(Exact.T.shape[1]*0.25)
    sample2 = int(Exact.T.shape[1]*0.50)
    sample3 = int(Exact.T.shape[1]*0.75)


    fig, ax = newfig(1.0, 1.1)
    ax.axis('off')
    
    ####### Row 0: u(t,x) ##################    
    gs0 = gridspec.GridSpec(1, 2)
    gs0.update(top=1-0.06, bottom=1-1/3, left=0.15, right=0.85, wspace=0)
    ax = plt.subplot(gs0[:, :])
    
    h = ax.imshow(U_pred.T, interpolation='nearest', cmap='rainbow', 
                  extent=[t.min(), t.max(), x.min(), x.max()], 
                  origin='lower', aspect='auto')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(h, cax=cax)
    
    ax.plot(X_u_train[:,1], X_u_train[:,0], 'kx', label = 'Data (%d pontos)' % (u_train.shape[0]), markersize = 4, clip_on = False)
    
    line = np.linspace(x.min(), x.max(), 2)[:,None]
    ax.plot(t[sample1]*np.ones((2,1)), line, 'w-', linewidth = 1)
    ax.plot(t[sample2]*np.ones((2,1)), line, 'w-', linewidth = 1)
    ax.plot(t[sample3]*np.ones((2,1)), line, 'w-', linewidth = 1)    
    
    ax.set_xlabel('$t$')
    ax.set_ylabel('$x$')
    ax.legend(frameon=False, loc = 'best')
    ax.set_title('$u(t,x)$', fontsize = 10)
    
    ####### Row 1: u(t,x) slices ################## 
         
    gs1 = gridspec.GridSpec(1, 3)
    gs1.update(top=1-1/3, bottom=0, left=0.1, right=0.9, wspace=0.5)

    ax = plt.subplot(gs1[0, 0])
    ax.plot(x,Exact[sample1,:], 'b-', linewidth = 2, label = 'Exato')       
    ax.plot(x,U_pred[sample1,:], 'r--', linewidth = 2, label = 'Predito')
    ax.set_xlabel('$x$')
    ax.set_ylabel('$u(t,x)$')    
    ax.set_title('$t = 0.25$', fontsize = 10)
    ax.axis('square')
    ax.set_xlim([-1.1,1.1])
    ax.set_ylim([-1.1,1.1])
    
    ax = plt.subplot(gs1[0, 1])
    ax.plot(x,Exact[sample2,:], 'b-', linewidth = 2, label = 'Exato')       
    ax.plot(x,U_pred[sample2,:], 'r--', linewidth = 2, label = 'Predito')
    ax.set_xlabel('$x$')
    ax.set_ylabel('$u(t,x)$')
    ax.axis('square')
    ax.set_xlim([-1.1,1.1])
    ax.set_ylim([-1.1,1.1])
    ax.set_title('$t = 0.50$', fontsize = 10)
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.35), ncol=5, frameon=False)
    
    ax = plt.subplot(gs1[0, 2])
    ax.plot(x,Exact[sample3,:], 'b-', linewidth = 2, label = 'Exato')       
    ax.plot(x,U_pred[sample3,:], 'r--', linewidth = 2, label = 'Predito')
    ax.set_xlabel('$x$')
    ax.set_ylabel('$u(t,x)$')
    ax.axis('square')
    ax.set_xlim([-1.1,1.1])
    ax.set_ylim([-1.1,1.1])    
    ax.set_title('$t = 0.75$', fontsize = 10)

  
    Name = namer('PlotMaziar', 'BurgInv', elapsed, optimizer= Optimizer, it = Epochs, lr = lr)
    savefig('./Figures/{}'.format(Name))  

    plot_aux(U_pred, x, t, ('diff', 'previsto', 'instantes'), 'BurgInv', elapsed,\
             Exact = Exact, optimizer = Optimizer, it = Epochs, lr = lr)
    
    print('Terminado')


