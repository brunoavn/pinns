import sys
sys.path.insert(0, '../aux/')

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from plotting import newfig, savefig


elapsed = '0:22:47.903751' #input('Tempo da Rede neural: ')

x0 = -8
xf = 8
t0 = 0 
tf = 10

Exact = np.loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosExatos.txt'.format(elapsed))
Exact_sol = Exact
U_pred =  np.loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosPreditos.txt'.format(elapsed))

data_idn_t = np.loadtxt('./Data/Sol_BurgersVis_T.txt')

t_idn = data_idn_t.flatten()[:,None]

keep = 2/3
index = int(keep*t_idn.shape[0])

points = len(Exact_sol[:,1])

fig, ax = newfig(1.3, 2.1)
ax.axis('off')
    
######## Row 2: Pressure #######################
########      Exact p(t,x,y)     ########### 
gs = gridspec.GridSpec(2, 2)
gs.update(top=0.8, bottom=0.2, left=0.1, right=0.9, wspace=0.5)
ax = plt.subplot(gs[0, 0])
h = ax.imshow(Exact_sol, interpolation='nearest', cmap='jet', origin='lower', aspect='auto', extent = [t0,tf,x0,xf])
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)

fig.colorbar(h, cax=cax)
ax.set_xlabel('$t$')
ax.set_ylabel('$x$')
ax.set_title('Dinâmica Exata', fontsize = 10)

line = np.linspace(x0, xf, 2)[:,None]
ax.plot(t_idn[index]*np.ones((2,1)), line, 'w-', linewidth = 1)
 
########     Predicted p(t,x,y)     ########### 
ax = plt.subplot(gs[0, 1])
h = ax.imshow(U_pred, interpolation='nearest', cmap='jet', extent = [t0,tf,x0,xf], origin='lower', aspect='auto')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)

    
fig.colorbar(h, cax=cax)
ax.set_xlabel('$t$')
ax.set_ylabel('$x$')
ax.set_title('Dinâmica Aprendida', fontsize = 10)

line = np.linspace(x0, xf, 2)[:,None]
ax.plot(t_idn[index]*np.ones((2,1)), line, 'w-', linewidth = 1)

####### Nossos Plots ###############

ax = plt.subplot(gs[1,:])
Abs_Diff = abs(Exact_sol - U_pred)
h = ax.imshow(Abs_Diff, interpolation='nearest', cmap='seismic', extent = [t0,tf,x0,xf], origin='lower', aspect='auto')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)

fig.colorbar(h, cax = cax)
ax.set_title('Diferença absoluta', fontsize = 10)
#x = np.linspace(x0,xf,points)

#ax.plot(x, Exact_sol[:, 100], linewidth = 2, label='Dados exatos')
#ax.plot(x, U_pred[:, 100],'r--', linewidth=2,  label= 'Dados aprendidos')
#ax.legend()
#ax.set_xlabel('$x$')
#ax.set_ylabel('$u(x,t)$')

savefig('./Figures/Deep_Advec_{}'.format(elapsed))
plt.clf()
