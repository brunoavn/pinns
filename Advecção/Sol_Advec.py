###################################################################
################ Resolvendo Equação de Advecção ###################
###################################################################

from clawpack import pyclaw
from clawpack import riemann
from numpy import savetxt
from numpy import loadtxt
from numpy import linspace
from numpy import array
from numpy import exp, sin, pi

class Advec:
    
    """
    Resolve a eq. de Advecção e exporta os dados para a pasta _output no formato (t,x).
    
    Argumentos
    =========
        x0 : float
            Valor inicial do domínio.
        tf : float
            Tempo final da simulação.
        frames : integer
            Número de quadros a serem simulados.
        points : integer
            Número de pontos da simulação.
        xf : float (Opcional)
            Valor final do domínio. Se não for fornecido,
            o programa rodará com um domínio simétrico.
            Padrão é : False
    """
    
    def __init__ (self, x0, tf, frames, points, xf = False):
        self.x0 = x0
        self.tf = tf
        self.frames = frames
        self.points = points
        if xf == False:
            self.xf = -x0
        else:
            self.xf = xf
    
    def Solve_Advec(self):
        
        ## -- Iniciando o Solver -- ##
        solver = pyclaw.SharpClawSolver1D(riemann.advection_1D_py.advection_1D)
        solver.weno_order = 5
        solver.time_integrator = 'SSP104'
        
        solver.kernel_language = 'Python'

        ## -- Condições de Contorno -- ##
        solver.bc_lower[0] = 0
        solver.bc_upper[0] = 0

        ## -- Domínio -- ##
        x = pyclaw.Dimension(self.x0, self.xf, self.points, name='x')
        domain = pyclaw.Domain((x))

        ## -- Aplicando a Solução -- ##
        state = pyclaw.State(domain, solver.num_eqn)

        xc = state.grid.x.centers

        ## -- Condições iniciais -- ##
        
        state.q[0,:] = exp(-((xc))**2)
        #state.q[0,:] = exp(-0.5*(xc/0.1)**2) 
        #state.q[0,:] = -sin(pi*xc)
        #state.q[0,xc<=1] = xc[xc<=1]
        #state.q[0,xc>1] = 2-xc[xc>1]
        #state.q[0,xc>2] = 0*xc[xc>2]
        state.problem_data['u'] = 0.50 # Para definir se utiliza-se entropy fix (?)
	

        ## -- Iniciando o controlador da simulação -- ##
        controller = pyclaw.Controller()
        controller.outdid = './Advec/'
        controller.solution = pyclaw.Solution(state, domain)
        controller.solver = solver
        controller.num_output_times = self.frames # número de quadros da simulação
        controller.tfinal = self.tf # tempo total da simulação

        controller.write_aux_init = False
        controller.write_aux_always = False

        controller.solver.all_bcs = pyclaw.BC.extrap

        status = controller.run()

        ## - Final do programa	
        print(40*'#' + '\n' + 11*'-' + 'Solução Finalizada' + 11*'-' + '\n' + 40*'#')
        
        
    def Read_sol(self, export = False, PATH = './_output/fort.'):
        
        """
        Função que lê e exporta a solução gerada, no formato (t,x).
        
        Argumentos
        =========
            export : boolean, opcional 
                Define se a solução lida deve ser exportada para
                a pasta ./Data/.

            PATH : string, optional
                Destino onde estão salvas as soluções.
                Padrão é './_output/fort.'
        
        !!IMPORTANTE!! 
                    O reader lê os 5 primeiros valores,
                    que são apenas informações do solver
                    excluir na hora de usar. 'x[:, 5:]'              
        """
        
        localx = [PATH + 'q' + format(i, '04') for i in range(self.frames + 1)]
        x_i = array([loadtxt(localx[a], usecols=0) for a in range(self.frames + 1)])

        localt = [PATH + 't' + format(i, '04') for i in range(self.frames + 1)]
        t_i = array([loadtxt(localt[a], usecols=0) for a in range(self.frames + 1)])
        t_i = t_i[:,0]

        if export:
            eixo = linspace(self.x0, self.xf, self.points)
            savetxt('./Data/eixo[{}x{}].txt'.format(self.x0, self.xf), eixo)
            
            nameX = './Data/Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf)
            savetxt(nameX, x_i)
            
            nameT = './Data/tempo_tf{}.txt'.format(self.tf)
            savetxt(nameT,t_i)
        else:
            pass
        
        return x_i, t_i
    

    def Sample(self, step):

        """
        Função que realiza uma amostragem dos pontos para 
        contornar problemas de dimensão do Pyclaw.

        Argumentos
        =========
            step : integer
                Espaçamento entre as amostras do conjunto original
        !! IMPORTANTE !!
                        Esta função sobrescreve os arquivos
                        exportados originalmente pelo solver.
        """
        X = loadtxt('./Data/Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf))
        Xaxis = loadtxt('./Data/eixo[{}x{}].txt'.format(self.x0, self.xf))
        
        X_ = X[:,5:]
        X_sampled = array([X_[:, i] for i in range(0,self.points, step)]).T
        Xaxis_sampled = array([Xaxis[i] for i in range(0,self.points, step)])

        nameX = './Data/Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf)
        savetxt(nameX, X_sampled)
        nameAxis = './Data/eixo[{}x{}].txt'.format(self.x0, self.xf)
        savetxt(nameAxis, Xaxis_sampled)

        return X_sampled, Xaxis_sampled

    
    def Slice(self, expect):
        X = loadtxt('./Data/Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf))
        Xaxis = loadtxt('./Data/eixo[{}x{}].txt'.format(self.x0, self.xf))
        
        X_ = X[:,5:]
        for i in range(0,len(Xaxis)):
            if Xaxis[i] == expect:
                Xaxis_sliced = Xaxis[i:]
                X_sliced = X_[:,i:]

                nameX = './Data/NovoSol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf)
                savetxt(nameX, X_sliced)
                nameAxis = './Data/Novoeixo[{}x{}].txt'.format(expect, self.xf)
                savetxt(nameAxis, Xaxis_sliced)
                print('Achado o ponto esperado exato!')
                break
            else:
                test = expect - Xaxis[i]
                if test < 0:
                    Xaxis_sliced = Xaxis[i-1:]
                    X_sliced = X_[:, i-1:]

                    nameX = './Data/*Sol_Advec_frames-{}_time-{}.txt'.format(self.frames, self.tf)
                    savetxt(nameX, X_sliced)

                    nameAxis = './Data/eixo[{:.2f}*x{}].txt'.format(Xaxis_sliced[0], self.xf)

                    nameAxis = './Data/*eixo[{:.2f}x{}].txt'.format(Xaxis_sliced[0], self.xf)

                    savetxt(nameAxis, Xaxis_sliced)
                    print('O ponto exato não estava disponível, tomamos o ponto {:.2f}'.format(Xaxis_sliced[0]))
                    break
                else:
                    continue
