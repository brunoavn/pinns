###################################################################
############# Resolvendo Burgers Sem Viscosidade #################
###################################################################

from clawpack import pyclaw
from clawpack import riemann
from numpy import savetxt
from numpy import loadtxt
from numpy import linspace
from numpy import array
from numpy import sin, pi

class Burgers:

    """
    Resolve a eq. de Burgers para o caso inviscido e exporta os dados para a pasta _output.
    
    Argumentos
    =========
        x0 : float
            Valor inicial do domínio, como o domínio é simétrico x0 = xf.
        tf : float
            Tempo final da simulação.
        frames : integer
            Número de quadros a serem simulados.
        points : integer
            Número de pontos da simulação.
    """
    
    def __init__ (self, x0, tf, frames, points):
        self.x0 = x0
        self.tf = tf
        self.frames = frames
        self.points = points
    
    
    def Solve_Burgers(self):

        solver = pyclaw.SharpClawSolver1D(riemann.burgers_1D_py.burgers_1D)
        solver.weno_order = 5
        solver.time_integrator = 'SSP104'
        
        solver.kernel_language = 'Python'

        ## -- Domínio -- ##
        x = pyclaw.Dimension(-self.x0, self.x0, self.points, name='x')
        domain = pyclaw.Domain((x))

        ## -- Aplicando a Solução -- ##
        state = pyclaw.State(domain, solver.num_eqn)
        
        xc = state.grid.x.centers

        ## -- Condições iniciais -- ##

        state.q[0,:] = -sin((pi/8) * xc)
        #state.q[0,:] = -sin(pi * xc)
        state.problem_data['efix'] = True
        # Para definir se utiliza-se entropy fix (?)

        ## -- Iniciando o controlador da simulação -- ##
        controller = pyclaw.Controller()
        controller.solution = pyclaw.Solution(state, domain)
        controller.solver = solver
        controller.num_output_times = self.frames # número de frames da simulação
        controller.tfinal = self.tf # tempo total da simulação

        controller.write_aux_init = False
        controller.write_aux_always = False

        controller.solver.all_bcs = pyclaw.BC.extrap

        status = controller.run()

        print(40*'#' + '\n' + 11*'-' + 'Solução Finalizada' + 11*'-' + '\n' + 40*'#')
        
    def Read_sol(self, export = False, PATH = './_output/fort.'):
        
        """
        Função que lê e exporta a solução gerada.
        
        Argumentos
        =========
            export : boolean, opcional 
                Define se a solução lida deve ser exportada para
                a pasta ./Data/.

            PATH : string, optional
                Destino onde estão salvas as soluções.
                Padrão é './_output/fort.'
        
        !!IMPORTANTE!! 
                    O reader lê os 5 primeiros valores,
                    que são apenas informações do solver
                    excluir na hora de usar. 'x[:, 5:]'              
        """
        
        localx = [PATH + 'q' + format(i, '04') for i in range(self.frames + 1)]
        x_i = array([loadtxt(localx[a], usecols=0) for a in range(self.frames + 1)])

        localt = [PATH + 't' + format(i, '04') for i in range(self.frames + 1)]
        t_i = array([loadtxt(localt[a], usecols=0) for a in range(self.frames + 1)])
        t_i = t_i[:,0]

        if export:
            eixo = linspace(-self.x0, self.x0, self.points)
            savetxt('./Data/eixo[{}x{}].txt'.format(-self.x0, self.x0), eixo)
            
            nameX = './Data/Sol_Bur_frames-{}_time-{}.txt'.format(self.frames, self.tf)
            savetxt(nameX, x_i)
            
            nameT = './Data/tempo_tf{}.txt'.format(self.tf)
            savetxt(nameT,t_i)
        else:
            pass
        
        return x_i, t_i


    def Sample(self, step):

            """
            Função que realiza uma amostragem dos pontos para 
            contornar problemas de dimensão do Pyclaw.

            Argumentos
            =========
                step : integer
                    Espaçamento entre as amostras do conjunto original
            !! IMPORTANTE !!
                            Esta função sobrescreve os arquivos
                            exportados originalmente pelo solver.
            """
            X = loadtxt('./Data/Sol_Bur_frames-{}_time-{}.txt'.format(self.frames, self.tf))
            Xaxis = loadtxt('./Data/eixo[-{}x{}].txt'.format(self.x0, self.x0))
            
            X_ = X[:,5:]
            X_sampled = array([X_[:, i] for i in range(0,self.points, step)]).T
            Xaxis_sampled = array([Xaxis[i] for i in range(0,self.points, step)])

            nameX = './Data/Sol_Bur_frames-{}_time-{}.txt'.format(self.frames, self.tf)
            savetxt(nameX, X_sampled)
            nameAxis = './Data/eixo[-{}x{}].txt'.format(self.x0, self.x0)
            savetxt(nameAxis, Xaxis_sampled)

            return X_sampled, Xaxis_sampled
