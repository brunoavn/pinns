import numpy as np
import matplotlib.pyplot as plt

def namer(method, model, train, optimizer = 'default', it = 0, lr = 0):
    '''
    Define os nomes dos dados a serem exportados de maneira 
    automática.

    Argumentos
    =========
        method : str
            Informação extra sobre o tipo de dado exportado.
        model : str, opcional
            Problena que está sendo resolvido.
        train : float
            Tempo de treinamento da NN.
        optimizer : str, opcional
            Optimizador utilizado no treinamento.
            Padrão é : 'default' 
        it : int, opcional
            Número de iterações rodadas durante o treinamento.
            Padrão é : 0
        lr : int, opcional
            A taxa de aprendizado do optimizador utilizado.
            Padrão é : 0
    '''
    name = '{}_{}'.format(model, method)
    if optimizer != 'default':
        name = '{}({}it-{}lr)_{}'.format(optimizer, it, lr, name)
    name = 'NN_treino({})_{}'.format(train, name)
    return name

def plot_aux(Pred, X, T, method, model, train, optimizer = 'default', it = 0, Exact = 0, lr = 0):
    for i in range(len(method)):
        if method[i] == 'instantes':
            plt.figure(0)
            plt.plot(X, Exact[int(len(T)*0.25)], '-', linewidth=3)
            plt.plot(X, Pred[int(len(T)*0.25)], '--', color = 'k', linewidth=3)
            plt.plot(X, Exact[int(len(T)*0.50)], '-', linewidth=3)
            plt.plot(X, Pred[int(len(T)*0.50)], '--', color = 'k', linewidth=3)
            plt.plot(X, Exact[int(len(T)*0.75)], '-', linewidth=3)
            plt.plot(X, Pred[int(len(T)*0.75)], '--', color = 'k', linewidth=3)
            plt.plot(X, Exact[len(T)-1], '-', linewidth=3)
            plt.plot(X, Pred[len(T)-1], '--', color = 'k', linewidth=3)
            plt.title('Comportamento do sistema em diferentes instantes')
            #plt.legend((
            #            'Exato em t ={:.2f}'.format(T[int(len(T)*0.25)]),\
            #            'Previsto em t ={:.2f}'.format(T[int(len(T)*0.25)]),\
            #            'Exato em t ={:.2f}'.format(T[int(len(T)*0.5)]),\
            #            'Previsto em t ={:.2f}'.format(T[int(len(T)*0.5)]),\
            #            'Exato em t ={:.2f}'.format(T[int(len(T)*0.75)]),\
            #            'Previsto em t ={:.2f}'.format(T[int(len(T)*0.75)]),\
            #            'Exato em t ={:.2f}'.format(T[len(T)-1]),\
            #            'Previsto em t ={:.2f}'.format(T[len(T)-1])
            #            ))

            nome = namer(method[i], model, train, optimizer, it, lr)
            plt.savefig('./Figures/{}.png'.format(nome), dpi=300)
            plt.clf()
            print('{} - Exportado com sucesso'.format(method))
        
        elif method[i] == 'previsto':
            plt.figure(2)
            plt.imshow(Pred.T, aspect='auto', cmap='rainbow', extent=[T.min(), T.max(), X.min(), X.max()], origin='lower')
            nome = namer(method[i], model, train, optimizer, it, lr)
            plt.savefig('./Figures/{}.png'.format(nome), dpi=300)
            plt.clf()
            print('{} - Exportado com sucesso'.format(method))
        
        elif method[i] == 'diff':
            plt.figure(3)
            plt.imshow(np.abs(Exact.T - Pred.T), aspect='auto', cmap='seismic', extent=[T.min(), T.max(), X.min(), X.max()], origin='lower', interpolation='nearest')
            plt.colorbar()
            nome = namer(method[i], model, train, optimizer, it, lr)
            plt.savefig('./Figures/{}.png'.format(nome), dpi=300)
            plt.clf()
            print('{} - Exportado com sucesso'.format(method))

        else:
            print('Erro:: {} não é um plot válido'.format(method))
    print('-- Exportação de gráficos finalizada --')
