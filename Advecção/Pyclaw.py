import numpy as np

from clawpack import pyclaw
from clawpack import riemann
solver = pyclaw.ClawSolver1D(riemann.advection_1D)

## -- Domínio -- ##
domain = pyclaw.geometry.Domain([-1.0], [1.0], [10.0])

## -- Aplicando a Solução -- ##
solution = pyclaw.Solution(solver.num_eqn, domain)
state = solution.state

#xc = state.grid.p_centers[0] # Com esta linha, temos os pontos no centro da malha e funcionando
xc = state.grid.p_nodes[0] # Este retorna os pontos alinhados, mas temos um erro de dimensão, como corrigir?

## -- Condições iniciais -- ##
#state.q = np.zeros((1,11)) #tentei usar isso para corrigir o problema, mas nada feito
state.q[0,:] = np.sin( xc )
state.problem_data['u'] = 1.0

## -- Iniciando o controlador da simulação -- ##
controller = pyclaw.Controller()
controller.solution = solution
controller.solver = solver
controller.num_output_times = 30 # número de quadros da simulação
controller.tfinal = 1 # tempo final da simulação

controller.write_aux_init = False
controller.write_aux_always = False

## -- Condições de Contorno -- ##
controller.solver.all_bcs = pyclaw.BC.extrap

status = controller.run()
        

from clawpack.pyclaw import plot
plot.interactive_plot()
