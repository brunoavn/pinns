from numpy import loadtxt
from numpy import abs
from numpy import linalg

import matplotlib.pyplot as plt

Explore = input('Qual o tempo de treinamento da NN a ser analisada? ')


U_pred = loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosPreditos.txt'.format(Explore))
Exact  = loadtxt('./Data/NN_Export/NN_treino({})_BurgersVis_DadosExatos.txt'.format(Explore))

U_pred = U_pred.T
Exact = Exact.T

eixoX = loadtxt('./Data/Sol_BurgersVis2_X.txt')
eixoT = loadtxt('./Data/Sol_BurgersVis2_T.txt')


Abs_Diff = abs(Exact - U_pred)

Error_t = linalg.norm(Abs_Diff, axis=1)
Error_x = linalg.norm(Abs_Diff, axis=0)

plt.figure(1)
plt.plot(eixoT, Error_t)
plt.title('Erro ao longo de t')
plt.xlabel('t')
plt.savefig('./Figures/Exploring/Erro_t_BurgresVis{}s.png'.format(Explore), dpi=300)


plt.figure(2)
plt.plot(eixoX, Error_x)
plt.title('Erro ao longo de x')
plt.xlabel('x')
plt.savefig('./Figures/Exploring/Erro_x_BurgersVis{}s.png'.format(Explore), dpi=300)

#######################################################################
######################Plotando os erros################################
#######################################################################

for i in range(0,200,20):
    plt.figure(i)    
    plt.plot(eixoX, Exact[i], 'b-', linewidth=4)
    plt.plot(eixoX, U_pred[i], 'r--', linewidth=3)
    plt.legend(('Exato, Previsto'))
    plt.title('Diferença entre previsto e exato para o tempo {}s na NN {}s'.format(i/100, Explore))
    plt.xlabel('X')
    plt.savefig('./Figures/Exploring/NN_BurgersVis{}-corte{}s.png'.format(Explore, i/100), dpi=300)
print(20*'#')
print(5*'#' + 'Terminado' + 6*'#')
print(20*'#')

