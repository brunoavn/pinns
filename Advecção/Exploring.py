from numpy import loadtxt
from numpy import abs
from numpy import linalg

import matplotlib.pyplot as plt

Explore = input('Qual o tempo de treinamento da NN a ser analisada? ')
Explore = float(Explore) 


Ctrl_U_Pred = loadtxt('./Data/NN_Export/C_DadosPreditos.txt')
Ctrl_Exact = loadtxt('./Data/NN_Export/C_DadosExatos.txt')

Ctrl_eixoX = loadtxt('./Data/eixo[-1x1].txt')
Ctrl_eixoT = loadtxt('./Data/tempo_tf1.txt')

Ctrl_Abs_Diff = abs(Ctrl_Exact - Ctrl_U_Pred)
Ctrl_Error_t = linalg.norm(Ctrl_Abs_Diff, axis=1)
Ctrl_Error_x = linalg.norm(Ctrl_Abs_Diff, axis=0)


U_pred = loadtxt('./Data/NN_Export/NN_treino({:.1f})_Advec_DadosPreditos.txt'.format(Explore))
Exact  = loadtxt('./Data/NN_Export/NN_treino({:.1f})_Advec_DadosExatos.txt'.format(Explore))

eixoX = loadtxt('./Data/eixo[-1x1].txt')
eixoT = loadtxt('./Data/tempo_tf1.txt')


Abs_Diff = abs(Exact - U_pred)

Error_t = linalg.norm(Abs_Diff, axis=1)
Error_x = linalg.norm(Abs_Diff, axis=0)

Compare_Error_x = abs(Ctrl_Error_x - Error_x)
Compare_Error_t = abs(Ctrl_Error_t - Error_t)
#######################################################################
######################Plotando os erros################################
#######################################################################
#plt.figure(1)
#plt.plot(eixoT, Error_t)
#plt.title('Erro ao longo de t na rede {}s'.format(Explore))
#plt.xlabel('t')
#plt.savefig('./Figures/Exploring/Erro_t_{}s.png'.format(Explore), dpi=300)

plt.figure(11)
plt.plot(eixoT, Error_t, '-')
plt.plot(eixoT, Ctrl_Error_t,'--', color='black')
plt.plot(eixoT, Compare_Error_t, color='red')
plt.grid(True)
plt.legend(('Rede', 'Controle', 'Rede - Controle'))
plt.title('Erro ao longo de t na rede {}s'.format(Explore))
plt.xlabel('t')
plt.savefig('./Figures/Exploring/Erro_t_{}s.png'.format(Explore), dpi=300)

#plt.figure(2)
#plt.plot(eixoX, Error_x)
#plt.title('Erro ao longo de x na rede {}s'.format(Explore))
#plt.xlabel('x')
#plt.savefig('./Figures/Exploring/Erro_x_{}s.png'.format(Explore), dpi=300)

plt.figure(22)
plt.plot(eixoX, Error_x, '-')
plt.plot(eixoX, Ctrl_Error_x, '--', color='black')
plt.plot(eixoX, Compare_Error_x, color='red')
plt.grid(True)
plt.legend(('Rede', 'Controle', 'Rede - Controle'))
plt.title('Erro ao longo de x na rede {}s'.format(Explore))
plt.xlabel('x')
plt.savefig('./Figures/Exploring/Erro_x_{}s.png'.format(Explore), dpi=300)

#######################################################################
####################Plotando os instantes##############################
#######################################################################

#for i in range(0,100,20):
#    plt.figure(i)    
#    plt.plot(eixoX, Exact[i,:], 'b-', linewidth=4)
#    plt.plot(eixoX, U_pred[i,:], 'r--', linewidth=3)
#    plt.legend(('Exato, Previsto'))
#    plt.title('Diferença entre previsto e exato para o tempo {}s na NN {}s'.format(i/100, Explore))
#    plt.xlabel('X')
#    plt.savefig('./Figures/Exploring/NN_{}-corte{}s.png'.format(Explore, i/100), dpi=300)

print(20*'#')
print(5*'#' + 'Terminado' + 6*'#')
print(20*'#')



